$(function () {

    $(".btn").click(function () {

        var $noteSpan = $("span[class='note']");
        var username = $.trim($("input[name='username']").val());
        var password = $.trim($("input[name='password']").val());
        var verifyCode = $.trim($("input[name='verifyCode']").val());

        $noteSpan.html("");

        if(username == ''){
            $("input[name='username']").focus();
            $noteSpan.html("用户名不能为空！");
            return;
        }

        if(username.length > 18 || username.length < 6){
            $("input[name='username']").focus();
            $noteSpan.html("用户名小于6位或大于18位.");
            return;
        }


        if(password == ''){
            $("input[name='password']").focus();
            $noteSpan.html("密码不能为空！");
            return;
        }

        if(password.length > 18 || password.length < 6){
            $("input[name='password']").focus();
            $noteSpan.html("密码小于6位或大于18位.");
            return;
        }

        if(verifyCode.toLocaleUpperCase() != 'YJSF'){
            $("input[name='password']").focus();
            $noteSpan.html("验证码不正确！");
            return;
        }

    });

});